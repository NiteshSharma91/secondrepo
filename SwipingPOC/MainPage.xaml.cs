﻿using Microsoft.Toolkit.Uwp.UI.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace SwipingPOC
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private bool _isSwiped;
        public MainPage()
        {
            this.InitializeComponent();
        }

        private readonly ObservableCollection<MailItem> dataSource = new ObservableCollection<MailItem>();
        

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //base.OnNavigatedTo(e);
            for (int i = 0; i < 20; i++)
            {
                //sampleList.Items.Add(new MailItem() { Subject = "Subject", Text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit..." });
                dataSource.Add(new MailItem() { Subject = "subject" + i });
            }
        }
        private void SlidableListItem_RightCommandRequested(object sender, EventArgs e)
        {
            var slidableitem = sender as SlidableListItem;

            DisplayDeleteFileDialog(slidableitem);

         
           

        }




      


        private void SlidableListItem_LeftCommandRequested(object sender, EventArgs e)
        {
            //MessageDialog hh = new MessageDialog();


            var slidableitem = sender as SlidableListItem;
            var item = slidableitem.DataContext as MailItem;

        }

        private async void DisplayDeleteFileDialog(SlidableListItem slidableitem)
        {
            ContentDialog deleteFileDialog = new ContentDialog
            {
                Title = "Delete file permanently?",
                Content="Do you want to delete it?",
                //Content = "If you delete this file, you won't be able to recover it. Do you want to delete it?",
                PrimaryButtonText = "Delete",
                CloseButtonText = "Cancel"
            };

            ContentDialogResult result = await deleteFileDialog.ShowAsync();

            // Delete the file if the user clicked the primary button.
            /// Otherwise, do nothing.
            if (result == ContentDialogResult.Primary)
            {
                // Delete the file.
                //var item = slidableitem.DataContext as MailItem;
                //int index = lstview.Items.IndexOf(item);
                //dataSource.RemoveAt(index);
            }
            else
            {
                // The user clicked the CLoseButton, pressed ESC, Gamepad B, or the system back button.
                // Do nothing.
            }
        }

      

                private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            FlyoutBase.ShowAttachedFlyout((FrameworkElement)sender);
        }
    }
}
